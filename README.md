# SAMPLE API Document
These are some of my contribution in the Operations-Software API. User needs access to view the expected results. Some of them shows the returned json.


## Data Management Module

### 1. Capture Groups
**GET**

`https://https://api.ops-aws.stamina4space.upd.edu.ph/data_management/capture_groups<parameter/s>
https://https://api.ops-aws.stamina4space.upd.edu.ph/data_management/capture_groups/<capture_group_id>`

| Parameter | Description | Example |
| ----- | ----- | ----- |
| capture_group_id | Capture Group ID | `/D2_SMI_2018-12-04T045918_2018-12-04T235959` |
| description | Sort by description | `?description=in Palawan` |
| payload | Sort by payload | `?payload=SMI` |
| created_time | Get capture group products given date range of upload time. | `?from:created_time=2019-12-01&to:created_time=2019-12-25` |
| added_by | Sort by uploader | `?added_by=admin` |
| mission | Sort by capture groups the group is included | `?mission=In China` |
| capture_group_products | Sort capture groups by products it contains | `?capture_group_products=D2_HPT` |
| captures | Sort capture groups by captures it contains | `?captures=D2_HPT` |

Sample returned body: 
```
{
    "id": 7,
    "mission": "Major Fleet, China",
    "thumbnail": "http://localhost:8000/media/capture_group_products_uploaded_files/main_menu_parts.png",
    "capture_group_id": "D2_HPT_2019-06-04T050736022",
    "description": "Went to China.",
    "created_time": "2021-03-18T15:46:27Z",
    "payload": "HPT",
    "added_by": "admin",
    "captures": [
        {
            "capture_id": "D2_HPT_2019-06-04T050736022_N",
            "link": "http://localhost:8000/data_management/captures/D2_HPT_2019-06-04T050736022_N"
        },
        {
            "capture_id": "D2_HPT_2019-06-04T050736022_B",
            "link": "http://localhost:8000/data_management/captures/D2_HPT_2019-06-04T050736022_B"
        },
        {
            "capture_id": "D2_HPT_2019-06-04T050736022_G",
            "link": "http://localhost:8000/data_management/captures/D2_HPT_2019-06-04T050736022_G"
        },
        {
            "capture_id": "D2_HPT_2019-06-04T050736022_R",
            "link": "http://localhost:8000/data_management/captures/D2_HPT_2019-06-04T050736022_R"
        }
    ],
    "capture_group_products": [
        {
            "capture_group_product_id": "TEST",
            "product_type": "Stacked",
            "link": "http://localhost:8000/data_management/capture_group_products/TEST"
        },
        {
            "capture_group_product_id": "TEST_2",
            "product_type": "Stacked",
            "link": "http://localhost:8000/data_management/capture_group_products/TEST_2"
        },
        {
            "capture_group_product_id": "D2_HPT_2019-06-04T050736022",
            "product_type": "Stacked",
            "link": "http://localhost:8000/data_management/capture_group_products/D2_HPT_2019-06-04T050736022"
        }
    ]
}
```

### 2. Capture Group Products
**GET**

`https://https://api.ops-aws.stamina4space.upd.edu.ph/data_management/capture_group_products<parameter/s>
https://https://api.ops-aws.stamina4space.upd.edu.ph/data_management/capture_group_products/<capture_group_product_id>`

| Parameter | Description | Example |
| ----- | ----- | ----- |
| capture_group_product_id | Capture Group Product ID | `/D2_SMI_2018-12-04T045918_2018-12-04T235959` |
| capture_group_product_id | Sort by capture group product ID | `?capture_group_product_id=D2_SMI` |
| uploaded_by | Sort by uploader | `?uploaded_by=admin` |
| description | Sort by description | `?description=in Palawan` |
| upload_time | Get capture group products given date range of upload time. | `?from:upload_time=2019-12-01&to:upload_time=2019-12-25` |
| capture_group | Sort by capture group | `?capture_group=D2_SMI_2018-12-04T045918_2018-12-04T235959` |
| product_type | Sort by uploader | `?product_type=Mosaic` |
| image_tags | Include/exclude captures based on image tag/s | `image_tags=Urban,Coastal` |

Sample returned body: 
```
{
    "id": 12,
    "uploaded_by": "admin",
    "product_type": "Stacked",
    "capture_group": "D2_HPT_2019-06-04T050736022",
    "file": null,
    "thumbnail": "http://localhost:8000/media/capture_group_products_uploaded_files/done_450.PNG",
    "image_tags": ["Urban", "Vegetation"],
    "capture_group_product_id": "D2_HPT_2019-06-04T050736022",
    "md5sum": "",
    "description": "Captured in Palawan",
    "footprint": null,
    "upload_time": "2021-03-18T16:01:15.532901Z"
}
```


## Missions Management
### 1. Acquisition Missions

`https://api.ops.phl-microsat.upd.edu.ph/missions_management/missions<parameter/s>`

| Parameter | Description | Example |
| ----- | ----- | ----- |
| name | Get mission by name | `?name=sample` |
| created_time | Display created missions in date range | `?from:created_time=2019-12-01&to:created_time=2019-12-25` |
| capture_time | Display captured missions in date range | `?from:capture_time=2019-12-01&to:capture_time=2019-12-25` |
| satellite | Filter missions by satellite | `?satellite=Diwata-1` |
| status | Filter missions by status | `?status=Pending` |
| pointing_mode | Filter missions by pointing mode | `?pointing_mode=Target Pointing` |
| capture_groups | Filter missions by capture_groups | `?capture_groups=D2_SMI` |
| captures | Filter missions by captures it contains | `?captures=D2_SMI` |
| *Mixins* | Mix different filters by using **&** to succeeding parameters | `?satellite=Diwata-1&status=Pending&pointing_mode=Target Pointing` |
| search | Display data with related keyword | `search=keyword` |